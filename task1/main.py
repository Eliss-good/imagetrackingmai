import cv2
import numpy as np
import matplotlib.pyplot as plt


def maskHandler(src, mask):
    return cv2.bitwise_and(src, src, mask=mask)


def changeColor(src, mask, maskChangeColor):
    red_mask = cv2.bitwise_and(src, src, mask=mask)
    return cv2.add(red_mask, maskChangeColor)


def start():
    src = cv2.imread("image.jpg")

    lower_red = np.array([0, 50, 50])
    upper_red = np.array([20, 255, 255])
    mask_red = cv2.inRange(cv2.cvtColor(src, cv2.COLOR_BGR2HSV), lower_red, upper_red)

    green_color = np.array([60, 255, 255])
    green_mask = np.zeros_like(src)
    green_mask[mask_red != 0] = green_color

    maskImage = maskHandler(src, mask_red)
    changeColorImage = changeColor(src, mask_red, green_mask)

    while True:
        cv2.imshow("1", src)
        cv2.imshow("2", maskImage)
        cv2.imshow("3", changeColorImage)
        key = cv2.waitKey(0) & 0xFF
        if key == 27:
            break

    cv2.destroyAllWindows()


start()
